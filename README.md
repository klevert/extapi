Подключеие к Api и скачивание данных с сайта.  

# Api

https://developer.nytimes.com/docs/books-product/1/overview

# Site

https://www.nytimes.com/books/best-sellers/

# Меню

Api – начальная страница (для информации).

Bestsellers - страница, содержащаяся категории книг последнего списка Bestsellers, при нажатии на категорию отображается список книг данной категории. 

All categories - все категории книг

Если данных нет в БД, осуществляется запрос к api. Возможна пауза перед рендерингом страницы.