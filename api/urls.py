from django.urls import path, include, re_path

from api import views
from api.views import CatalogBookList, CatalogBookView, BestsellerList, BestsellerView

urlpatterns = [
    path('', views.index, name='index'),
    re_path(r'^book/catalog$', CatalogBookList.as_view(), name='book_catalog'),
    path('book/catalog/<int:pk>', CatalogBookView.as_view(), name='book_catalog_view'),
    re_path(r'^book/bestseller$', BestsellerList.as_view(), name='book_bestseller'),
    path('book/bestseller/<int:pk>/<str:dt>/', BestsellerView.as_view(), name='book_bestseller_view'),
]
