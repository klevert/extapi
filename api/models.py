from django.db import models


class CatalogBookModel(models.Model):
    class Meta:
        db_table = 'catalog_book'
        ordering = ['display_name']

    list_name = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255)
    oldest_published_date = models.DateField(null=True)
    newest_published_date = models.DateField(null=True)

    def __str__(self):
        return self.display_name


class BestsellerBookModel(models.Model):
    class Meta:
        db_table = 'bestseller_book'

    bestsellers_date = models.DateField(null=True)
    published_date = models.DateField(null=True)
    last_modified = models.DateTimeField(null=True)
    rank = models.PositiveIntegerField(null=True)
    book = models.ForeignKey('BookModel', on_delete=models.CASCADE)
    catalog = models.ForeignKey('CatalogBookModel', on_delete=models.CASCADE)


class BookModel(models.Model):
    class Meta:
        db_table = 'book'
        ordering = ['title']

    title = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=255, null=True)
    author = models.CharField(max_length=255, null=True)
    isbn13 = models.CharField(max_length=13, null=True)

    def __str__(self):
        return f"{self.title}, {self.author}"
