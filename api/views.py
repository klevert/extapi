import datetime
import requests
from django.http import Http404
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.views import generic
from api.models import CatalogBookModel, BookModel, BestsellerBookModel
from extapi.settings import API_KEY


def index(request):
    return TemplateResponse(request, "index.html")


class CatalogBookList(generic.ListView):
    model = CatalogBookModel
    template_name = 'catalog_book/list.html'
    ordering = ['display_name']
    paginate_by = 15

    def get_queryset(self):
        sunday = get_data_next_sunday()
        max_date = CatalogBookModel.objects.order_by('-newest_published_date').first()
        if not (max_date and sunday == max_date.newest_published_date):
            json_parser_catalog()
        return CatalogBookModel.objects.all()


class CatalogBookView(generic.DetailView):
    model = CatalogBookModel
    template_name = 'catalog_book/view.html'
    fields = [
        'list_name',
        'oldest_published_date',
        'newest_published_date',
        'created_at',
    ]

    def get(self, request, *args, **kwargs):
        return get_404(self, 'book_catalog', request, *args, **kwargs)


class BestsellerList(generic.ListView):
    model = CatalogBookModel
    template_name = 'bestseller/list.html'
    ordering = ['display_name']
    paginate_by = 15

    def get_queryset(self):
        sunday = get_data_next_sunday()
        max_date = CatalogBookModel.objects.order_by('-newest_published_date').first()
        if not (max_date and sunday == max_date.newest_published_date):
            json_parser_catalog()
            max_date = CatalogBookModel.objects.order_by('-newest_published_date').first()
        self.max_date = max_date.newest_published_date
        return CatalogBookModel.objects.filter(newest_published_date=max_date.newest_published_date)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['max_date'] = self.max_date
        return data


class BestsellerView(generic.DetailView):
    model = CatalogBookModel
    template_name = 'bestseller/view.html'
    fields = [
        'list_name',
        'oldest_published_date',
        'newest_published_date',
        'created_at',
    ]

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        id = self.kwargs['pk']
        max_date = datetime.datetime.fromisoformat(self.kwargs['dt'])
        data['bestseller'] = BestsellerBookModel.objects.filter(catalog=id, published_date=max_date).order_by("rank")
        if not data['bestseller']:
            json_parser_list(self.object.list_name)
            data['bestseller'] = BestsellerBookModel.objects.filter(catalog=id).order_by("rank")
        data['max_date'] = max_date
        return data


def get_data_next_sunday():
    today = datetime.date.today()
    weekday = today.weekday() + 1
    return today + datetime.timedelta(days=(7 - weekday % 7))


def json_parser_catalog():
    url = 'https://api.nytimes.com/svc/books/v3/lists/names.json'
    params = {"api-key": API_KEY}

    r = requests.get(url, params=params)
    if r.status_code != 200:
        return {"error": 'error status_code != 200'}
    results = r.json()['results']

    log = {
        "Добавлено": 0,
        "Изменено": 0,
    }

    for result in results:
        obj, created = CatalogBookModel.objects.update_or_create(
            list_name=result['list_name'],
            defaults={
                'list_name': result['list_name'],
                'display_name': result['display_name'],
                'oldest_published_date': result['oldest_published_date'],
                'newest_published_date': result['newest_published_date'],
            },
        )
        if created:
            log["Добавлено"] += 1
        else:
            log["Изменено"] += 1
    return log


def json_parser_list(catalog):
    url = 'https://api.nytimes.com/svc/books/v3/lists.json'
    params = {"api-key": API_KEY, "list_name": catalog}

    r = requests.get(url, params=params)
    if r.status_code != 200:
        return {"error": 'error status_code != 200'}
    results = r.json()
    for result in results['results']:
        book, created = BookModel.objects.get_or_create(
            isbn13=result['isbns'][0]['isbn13'],
            defaults={
                'title': result['book_details'][0]['title'],
                'description': result['book_details'][0]['description'],
                'author': result['book_details'][0]['author'],
                'isbn13': result['book_details'][0]['primary_isbn13'],
            },
        )

        catalog = CatalogBookModel.objects.get(list_name=result['list_name'])
        BestsellerBookModel.objects.get_or_create(
            last_modified=results['last_modified'],
            book=book,
            catalog=catalog,
            defaults={
                'bestsellers_date': result['bestsellers_date'],
                'published_date': result['published_date'],
                'last_modified': results['last_modified'],
                'rank': result['rank'],
                'catalog': catalog,
                'book': book,
            },
        )


def get_404(obj, redir, request, *args, **kwargs):
    try:
        obj.object = obj.get_object()
    except (Http404, ValueError):
        return redirect(redir)

    context = obj.get_context_data(object=obj.object)
    return obj.render_to_response(context)
